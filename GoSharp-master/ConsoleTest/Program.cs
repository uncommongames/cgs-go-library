using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Go;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //KillTest();
            TestScoring();
        }

        private static void KillTest()
        {
            var gi = new GameInfo();
            var g = new Game(gi);

            g.SetupMove(1, 0, Content.Black);
            g.SetupMove(1, 1, Content.Black);
            g.SetupMove(1, 2, Content.Black);
            g.SetupMove(2, 3, Content.Black);
            g.SetupMove(3, 0, Content.Black);
            g.SetupMove(3, 3, Content.Black);
            g.SetupMove(4, 2, Content.Black);
            g.SetupMove(5, 2, Content.Black);
            g.SetupMove(6, 2, Content.Black);
            g.SetupMove(7, 0, Content.Black);
            g.SetupMove(7, 1, Content.Black);
            g.SetupMove(7, 2, Content.Black);

            g.SetupMove(2, 0, Content.White);
            g.SetupMove(2, 1, Content.White);
            g.SetupMove(2, 2, Content.White);
            g.SetupMove(3, 2, Content.White);
            g.SetupMove(4, 0, Content.White);
            g.SetupMove(4, 1, Content.White);
            g.SetupMove(5, 0, Content.White);
            g.SetupMove(5, 1, Content.White);
            g.SetupMove(6, 0, Content.White);
            g.SetupMove(6, 1, Content.White);


            Console.WriteLine("{0}", g.Board);
            var result = g.MakeMove(3, 1);
            Console.WriteLine("{0}", result.Board);

            Console.ReadLine();
        }

        static void TestScoring()
        {
            // reading sgf file need to be read in invariant culture
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            //var game = LoadSgf("19954222-197-Samick-Racines");
            var game = LoadSgf("9x9");
            Console.WriteLine("{0}", game.Board);

            // if each player has passed
            if ((!game.Move.HasValue && !game.PrevMove.Move.HasValue) ||
                (game.Move.HasValue && game.Move.Value.Equals(Game.PassMove) && game.PrevMove.Move.HasValue && game.PrevMove.Move.Value.Equals(Game.PassMove)))
            {
                game.Board.IsScoring = true;
                ShowTerritory(game);

                Console.WriteLine("Set dead group");
                game.Board.SetDeadGroup(2, 5);
                //game.Board.SetDeadGroup(17, 2);
                //game.Board.SetDeadGroup(15, 17);
                //game.Board.SetDeadGroup(17, 16);
                ShowTerritory(game);
                //Console.WriteLine("{0}", game.Board);
                ShowScore(game);
            }

            Console.ReadLine();
        }

        private static void ShowBoardScore(Game game)
        {
            string[,] res = new string[game.Board.SizeX, game.Board.SizeY];

            string[] spaceContentStr = new string[] { "*", "x", "o" };
            string[] contentStr = new string[] { ".", "X", "O" };

            foreach (var g in game.Board.Groups)
            {
                foreach (var p in g.Points)
                {
                    if (g.Content == Content.Empty)
                        res[p.x, p.y] = spaceContentStr[(int)g.Territory];
                    else if (g.IsDead)
                    {
                        if (g.Content == Content.Black)
                            res[p.x, p.y] = spaceContentStr[(int)Content.White];
                        else 
                            res[p.x, p.y] = spaceContentStr[(int)Content.Black];
                    }
                    else
                        res[p.x, p.y] = contentStr[(int)g.Content];
                }
            }

            string resStr = "";
            for (int y = 0; y < res.GetLength(1); y++)
            {
                string line = "";
                for (int x = 0; x < res.GetLength(0); x++)
                {
                    line += res[x, y] + " ";
                }
                resStr += line + "\n";
            }

            Console.WriteLine(resStr);
        }

        private static void ShowScore(Game game)
        {
            int blackTerritory = game.Board.Territory[Go.Content.Black];
            int whiteTerritory = game.Board.Territory[Go.Content.White];

            ShowBoardScore(game);

            var blackPoint = game.BlackCaptures + blackTerritory;
            var whitePoint = game.WhiteCaptures + whiteTerritory + game.Root.GameInfo.Komi;

            Console.WriteLine("Black points = " + blackPoint);
            Console.WriteLine("White points = " + whitePoint);
        }

        private static void ShowTerritory(Game game)
        {
            int blackTerritory = game.Board.Territory[Go.Content.Black];
            int whiteTerritory = game.Board.Territory[Go.Content.White];

            Console.WriteLine("Black Territory = " + blackTerritory);
            Console.WriteLine("White Territory = " + whiteTerritory);
        }

        static Go.Game LoadSgf(string filename)
        {
            // Load the last move from sgf file
            var sgf = Go.Game.SerializeFromSGF(@"E:\Autres\Workspace 2019\GoColor\GoColor_2019\Rersources\sgf\" + filename + ".sgf");
            return ShowLastMove(sgf[sgf.Count - 1]);
        }

        static Go.Game ShowLastMove(Go.Game game)
        {
            bool hasNextMove = false;
            do
            {
                hasNextMove = false;
                foreach (var m in game.Moves)
                {
                    game = m;
                    hasNextMove = true;
                    break;
                }
            } while (hasNextMove);

            return game;
        }
    }
}
